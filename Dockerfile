FROM adoptopenjdk/openjdk11:ubi
RUN mkdir /opt/app
COPY marvel-0.0.1-SNAPSHOT.jar /opt/app
CMD ["java", "-Dspring.profiles.active=${SPRING_PROFILE}",  "-jar", "/opt/app/marvel-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080